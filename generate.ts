import { writeFileSync } from "fs"
import {
  setSaturation,
  setLightness,
  setHue,
  lighten,
  rotate,
  saturate
} from "unitransform"

const PURE_COLOR = "#ff0000"

const SATURATION_BG = 15
const SATURATION_WASHED = 50
const SATURATION_BASE = 70
const SATURATION_IMPORTANT = 80

const LUM_BG = 12
const LUM_DARK = 65
const LUM_BASE = 75
const LUM_BRIGHT = 100

const HUE_ERROR = 0
const HUE_COMMENT = 120
const HUE_KEYWORD = 240
const HUE_VARIABLE = 225
const HUE_FUNCTION = 60
const HUE_TYPE = 180
const HUE_STRING = -5
const HUE_CONSTANT = 270
const HUE_KEY = 200
const HUE_ESCAPE = 300

const BG = hsl(120, SATURATION_BG, LUM_BG)
const GREEN = hsl(120, 50, 50)
const WHITE = "#ffffff"
const DEFAULT_FG = setLightness(WHITE, LUM_BASE)
const DARKEST_BG = hsl(120, SATURATION_BG, LUM_BG - 4)
const CONTROL_INACTIVE_BG = lighten(BG, 5)
const CONTROL_BG = saturate(CONTROL_INACTIVE_BG, 30)
const LIST_HIGHLIGHT = lighten(saturate(rotate(CONTROL_BG, -30), 5), 4)
const LIST_ACTIVE = rotate(CONTROL_BG, 30)
const CONTROL_HOVER_BG = rotate(lighten(BG, 15), -30)
const CONTROL_FOCUS_BG = saturate(CONTROL_HOVER_BG, 40)

if (require.main === module) {
  const theme: any = {
    name: "Kokuban",
    type: "dark",
    colors: {
      "activityBar.background": DARKEST_BG,
      "dropdown.background": DARKEST_BG,
      "editor.background": BG,
      "editor.foreground": DEFAULT_FG,
      foreground: setLightness(WHITE, LUM_BRIGHT),
      "input.background": CONTROL_BG,
      "list.activeSelectionBackground": LIST_ACTIVE,
      "list.hoverBackground": LIST_HIGHLIGHT,
      "list.inactiveSelectionBackground": CONTROL_INACTIVE_BG,
      "sideBar.background": DARKEST_BG,
      "titleBar.activeBackground": DARKEST_BG, // title bar
      "list.focusBackground": CONTROL_FOCUS_BG,
      "editorGroupHeader.tabsBackground": DARKEST_BG,
      "editorBracketMatch.border": GREEN,
      "editorBracketMatch.background": DARKEST_BG,
      "tab.inactiveBackground": DARKEST_BG,
      "peekViewEditor.background": rotate(BG, 120),
      "titleBar.inactiveBackground": DARKEST_BG,
      "titleBar.inactiveForeground": setLightness(WHITE, LUM_DARK),
      "tab.border": CONTROL_INACTIVE_BG,
      "sideBarSectionHeader.background": saturate(CONTROL_BG, 10)
    },
    tokenColors: Array.from(generateTokenColors())
      .map(([name, scope, settings]) => {
        return {
          name,
          scope,
          settings:
            typeof settings === "string" ? { foreground: settings } : settings
        }
      })
      .concat(Array.from(jsonKeys()))
  }

  writeFileSync(
    `${__dirname}/themes/Kokuban-color-theme.json`,
    JSON.stringify(theme, null, 2)
  )
  console.log("Theme generated.")
}

function* generateTokenColors(): IterableIterator<
  [string, string | string[], string | {}]
> {
  yield ["Comment", ["comment"], hsl(HUE_COMMENT, SATURATION_WASHED, LUM_DARK)]

  yield [
    "Reset",
    [
      "keyword.operator",
      "storage.type.function.arrow.ts",
      "string.template meta.template.expression",
      "source.python punctuation.definition.arguments",
      "source.python meta.function punctuation",
      "source.python punctuation.section.function.begin",
      "source.python punctuation.definition.list",
      "source.python meta.function-call.arguments"
    ],
    DEFAULT_FG
  ]

  yield [
    "Keyword",
    [
      ...["js", "jsx", "ts", "tsx"].flatMap(lang => [
        ...[
          "in",
          "instanceof",
          "of",
          "typeof",
          "void",
          "is",
          "infer",
          "keyof"
        ].map(k => `keyword.operator.expression.${k}.${lang}`),
        `keyword.operator.new.${lang}`,
        `storage.type.class.${lang}`,
        `storage.type.function.${lang}`,
        `storage.type.interface.${lang}`,
        `storage.type.${lang}`,
        `storage.type.type.${lang}`,
        `storage.type.builtin.${lang}`,
        `storage.type.namespace.${lang}`,
        `support.type.builtin.${lang}`
      ]),
      "entity.name.tag",
      "heading.markdown",
      "keyword",
      "punctuation.decorator",
      "storage.modifier",
      "support.type.primitive", // primitive type
      "variable.language", // this, etc.
      "source.kotlin storage.type",
      "source.python meta.function",
      "source.python storage.type.function",
      "source.python storage.type.class",
      "source.python constant.language",
      "source.python keyword.operator.logical"
    ],
    hsl(HUE_KEYWORD, SATURATION_BASE, LUM_DARK)
  ]

  yield [
    "Link",
    ["markup.underline.link"],
    hsl(HUE_KEYWORD, SATURATION_IMPORTANT, LUM_BRIGHT)
  ]

  yield [
    "Adjustment for constants",
    ["constant.language", ""],
    hsl(HUE_CONSTANT, SATURATION_BASE, LUM_BASE)
  ]

  yield [
    "Variable",
    [
      "variable",
      "entity.name.type.anchor.yaml",
      "support.variable",
      "entity.name.variable.cs",
      ...["js", "jsx", "ts", "tsx"].flatMap(lang => [
        `meta.function-call support.class.console.${lang}`
      ])
    ],
    hsl(HUE_VARIABLE, 0, LUM_BRIGHT)
  ]

  yield [
    "Invalid",
    ["invalid"],
    hsl(HUE_ERROR, SATURATION_IMPORTANT, LUM_BRIGHT)
  ]

  yield [
    "Function",
    [
      "entity.name.function",
      "support.function",
      "markup.quote.markdown",
      "source.python meta.function-call"
    ],
    hsl(HUE_FUNCTION)
  ]

  yield [
    "Type",
    [
      "storage.type",
      "entity.name.type",
      "entity.other.attribute-name.class.css",
      "support.class",
      "entity.other.inherited-class",
      "support.constant.json",
      "support.constant.math",
      "storage.type.cs"
    ],
    hsl(HUE_TYPE)
  ]

  yield [
    "Type (highlight)",
    [
      "meta.object.type.tsx punctuation.definition.block.tsx",
      "meta.object.type.ts punctuation.definition.block.ts"
    ],
    hsl(HUE_TYPE, SATURATION_IMPORTANT)
  ]

  yield [
    "Namespace",
    ["entity.name.type.namespace"],
    hsl(HUE_TYPE, SATURATION_BASE, LUM_DARK)
  ]

  yield [
    "String",
    ["string", "keyword.control.flow.block-scalar.yaml"],
    hsl(HUE_STRING, SATURATION_IMPORTANT, LUM_BASE)
  ]

  yield [
    "Escape",
    [
      "constant.character.escape",
      "markup.inline.raw.string.markdown",
      "constant.other.placeholder.c",
      "text.html constant.character.entity",
      "meta.tag.metadata.cdata.html punctuation.definition.tag.begin.html",
      "meta.tag.metadata.cdata.html punctuation.definition.tag.end.html",
      "string.template meta.template.expression punctuation.definition.template-expression",
      "source.python string.quoted constant.language"
    ],
    hsl(HUE_ESCAPE, SATURATION_IMPORTANT, LUM_BASE)
  ]

  yield [
    "Constant",
    ["constant", "support.constant"],
    hsl(HUE_CONSTANT, SATURATION_IMPORTANT, LUM_BRIGHT)
  ]

  yield [
    "Key",
    [
      "entity.name",
      "meta.object-literal.key",
      "entity.other.attribute-name", // HTML, JSX
      "support.type.property-name", // CSS, JSON
      "meta.hashtable.assignment.powershell",
      "variable.object.property.tsx",
      "variable.object.property.ts"
    ],
    hsl(HUE_KEY, SATURATION_WASHED, LUM_DARK)
  ]

  yield [
    "Bold",
    ["markup.bold", "markup.heading"],
    {
      fontStyle: "bold"
    }
  ]

  yield [
    "Italic",
    ["markup.italic"],
    {
      fontStyle: "italic"
    }
  ]

  yield [
    "Meta",
    [
      // deviate from white to give character to id matches
      "entity.other.attribute-name.id.css",
      "keyword.control.export",
      "keyword.control.import",
      "support.type.object.module",
      "keyword.control.directive.include",
      "meta.preprocessor",
      "keyword.preprocessor.cs"
    ],
    hsl(HUE_ESCAPE, SATURATION_BASE, LUM_DARK)
  ]
}

function hsl(
  hue: number,
  saturation: number = SATURATION_BASE,
  lightness: number = LUM_BASE
): string {
  return setLightness(
    setSaturation(setHue(PURE_COLOR, hue), saturation),
    lightness
  )
}

function* jsonKeys() {
  const LEVELS = 6
  const hueIncrement = 360 / LEVELS
  const hueBegin = 300
  for (let i = 0; i < LEVELS; i++) {
    yield {
      name: `JSON Key - Level ${i}`,
      scope:
        "source.json" +
        multiplyString(
          " meta.structure.dictionary.json meta.structure.dictionary.value.json",
          i
        ) +
        " meta.structure.dictionary.json support.type.property-name.json",
      settings: {
        foreground: hsl(hueBegin - hueIncrement * i, SATURATION_BASE, LUM_DARK)
      }
    }
  }
}

function multiplyString(str: string, mult: number): string {
  return Array.from({ length: mult })
    .map(() => str)
    .join("")
}
