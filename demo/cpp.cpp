#include <iostream>

// line comment
/*
  Block comment */
class myClass {
  public:

  myClass(int);

  private:

  int i;
};

myClass::myClass(int x): i(x) {}

int main() {
  myClass my_class(5);

  myClass* my_class_ptr = new myClass(5);
  delete my_class_ptr;
  std::cout << std::endl;

  return 0;
}
