package optimumpath;

import java.io.BufferedReader;
import java.io.FileReader;

fun main() {
  Runner()
}

class Runner {

  val rows: MutableList<List<Int>> = mutableListOf()
  var rowCount = 0
  var colCount = 0

  constructor() {
    val reader = BufferedReader(FileReader("input.txt"))

    for (line in reader.lineSequence()) {
      println(line)
    }
  }

  fun <T> MutableList<T>.pop(): T {
    if (this.size == 0) {
      throw IndexOutOfBoundsException()
    }
    val last = this.last()
    this.removeAt(this.lastIndex)
    return last
  }
}
