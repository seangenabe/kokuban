import { NgModule } from "@angular/core"
import { BrowserModule } from "@angular/platform-browser"
import { FormsModule } from "@angular/forms"

import { AppComponent } from "./app.component"

// line comment
/*
 * block comment
 * @param {number} param
 */
@NgModule({
  imports: [BrowserModule, FormsModule],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
  _undefined: undefined = undefined
  _null: null = null
  _void: void = undefined
  _unknown: unknown = undefined
  _any: any | (unknown & {}) = undefined

  constructor() {}

  foo(unused: number, param2: Other) {
    param2
    this.bar()
    new Other()
  }

  bar() {
    console.log("bonk!s\t\r\n\u{1234}" + `abc${__filename}def` + `${1 + 1}`)
  }
}

class Other {}
class Unused {}

export function exportedFunction() {}
function nonExportedFunction() {}
nonExportedFunction()
export const exportedVariable = 1
const nonExportedVariable = 9
nonExportedVariable
const unusedVariable = Symbol()
const functionAsConst = () => 2
functionAsConst
;/some\sregex[0-9]*c?.+/gim
;({
  ab: 1,
  [Symbol.iterator]: "blah",
  func() {
    true && false
    return 1 + 2 || 3 ^ (4 & (5 * 6))
  }
})
const symbol: unique symbol = Symbol()
type Keyof = keyof Other

Number(5)
new Number(5)
const date: Date = new Date()
new Intl.DateTimeFormat()
Intl.DateTimeFormat()
0b01
0x01
0o01
1e9

undefined
null
Infinity
NaN
true
false
void 0
typeof 4
;({} instanceof Other)
RegExp
Reflect.apply
ReferenceError
RangeError
Promise
Promise.all([])
"keys" in Object
for (let x of []) {
}
type T1 = { [key in keyof {}]: string }
type T2 = typeof unusedVariable

encodeURIComponent("a")
JSON
JSON.parse("{}")
Math
Math.PI
Map
Set
WeakMap
WeakSet
Int16Array
Int32Array
Uint16Array
Uint32Array
Uint8Array
Uint8ClampedArray
Float32Array
Float64Array
setTimeout(() => void 0)
setImmediate(() => void 0)
setInterval(() => void 0)
alert()
prompt()
require("fs")
require.main
module
module.exports
module.exports.x = 1
;`actually in template string${Boolean(require) && Boolean(alert())}`
