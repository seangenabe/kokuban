#include <stdio.h>
#include <conio.h>

int main() {
  clrscr();
  int n, i;
  printf("Enter a number: ");
  scanf("%d", &n);
  for (i = 0; i < n; i++) {
    printf("You're beautiful!\n");
  }
  getch();
  return 0;
}

void foo() {
  _Bool a;
  char b;
  signed char c;
  short d;
  int e;
  long f;
  unsigned long long g;
  float h;
  double i;
  long double j;
  int k = &a;
  int l = *k;
  if (true) {} else {}
  while (false) {
    label:
    continue
    break
    goto label;
    int *labelptr = &label;
    NULL;
  }
}

// Comment
/*
  Block comment
  */
enum directions { NORTH } dir;
struct JJ {};
